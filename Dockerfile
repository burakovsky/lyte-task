FROM python:3.7-alpine3.10

ENV APP_HOST 0.0.0.0
ENV APP_PORT 8000

COPY requirements.txt .
RUN pip install --requirement requirements.txt
COPY server.py .

ENTRYPOINT ["python"]
CMD ["server.py"]
EXPOSE $APP_PORT
