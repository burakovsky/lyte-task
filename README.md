## Lyte test application repository

## Challenge

### Prepare project

1. Create your own repository and move this project to it ([here we are](https://gitlab.com/burakovsky/lyte-task))
2. Set up CI job runs tests on each push to project ([see test stage](https://gitlab.com/burakovsky/lyte-task/-/blob/master/.gitlab-ci.yml))
3. Write Dockerfile that creates Docker image for this mini-project ([Dockerfile](https://gitlab.com/burakovsky/lyte-task/-/blob/master/Dockerfile))
4. Write CI job that builds Docker image and push it to the registry by pushing up a new Git-tag with version. ([see build stage](https://gitlab.com/burakovsky/lyte-task/-/blob/master/.gitlab-ci.yml))

### Setting up application

1. Nginx as a reverse-proxy and DMZ should listen on 80 port
2. Server should work behind Nginx

### Build infrastructure

We are big fans of Terraform as an IaC approach. Could you do the next things:

1. Create a new repository contains terraform scripts
2. Set up infrastructure: EC2 instance for our application
3. Deployment scripts to deploy this small application from docker registry

### Advantages

* Setting up logs in AWS CloudWatch
* Setting up continuous delivery

## Some details

1. I choose ECR as Docker registry because:
* there was no particular Docker registry in task definition
* we need to deploy application to EC2, so it's easier to access ECR from EC2 instance then any other registry
2. I used [NGINX reverse proxy for Docker](https://github.com/nginx-proxy/nginx-proxy) as a reverse proxy (I should use it if I understood task correctly). I used EC2 instance public IP as VIRTUAL_HOST variable for application container. Both, nginx container and application container, are run on one instance (according task definition).
3. [Terraform repo](https://gitlab.com/burakovsky/lyte-terraform-task)
