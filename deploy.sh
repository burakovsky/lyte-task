#!/bin/bash

# ECR login
ssh ec2-user@$HOST "aws ecr get-login --no-include-email --region us-east-1 | source /dev/stdin"

# Delete app docksre container if exists
ssh ec2-user@$HOST "docker ps -q --filter 'name=app' | grep -q . && docker stop app && docker rm -fv app || true"

# Pull image and run Cocker container
ssh ec2-user@$HOST "docker pull $REPOSITORY_URL:$CI_COMMIT_TAG && docker run -d --name=app -e VIRTUAL_HOST=$HOST \
  --log-driver="awslogs" --log-opt awslogs-region="us-east-1" --log-opt awslogs-group="/ec2/lyte-app" --log-opt awslogs-stream="app-auto" $REPOSITORY_URL:$CI_COMMIT_TAG && exit"